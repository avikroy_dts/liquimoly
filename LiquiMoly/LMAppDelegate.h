//
//  LMAppDelegate.h
//  LiquiMoly
//
//  Created by Avik Roy on 7/26/13.
//  Copyright (c) 2013 Avik Roy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LMViewController;

@interface LMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LMViewController *viewController;

@end
